import json
from alakantti.lib.jcd_client import JCDecaux

class PointOfInterest(object):
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude
        
class AdvertisementOutdoor(PointOfInterest):
    pass
    
class JCDEcauxOutdoor(AdvertisementOutdoor):
    @classmethod
    def get_all(kls):
        hubs = JCDecaux().get_hubs().json()
        return (JCDEcauxOutdoor(hub) for hub in hubs)
        
    def populate_detail(kls, hub_id):
        hub_detail = JCDecaux().get_frame_info(self.id).json()
        
        self.porvider_impressions = hub_detail['impressions']['days']
    
    def __init__(self, json):
        self.id = json['id']
        self.name = json['name']
        self.demographics = json['demographics']
        self.price = json['price']
        self.latitude = json['latitude']
        self.longitude = json['longitude']
        
        self.units = json['hubCpt']
        self.provider_throughput = json['otc']
        self.selected = False
    
    @classmethod
    def json_render_datapoint_list(kls, object_list):
        return json.dumps(list(x.__dict__ for x in object_list))