import os
from influxdb import InfluxDBClient

class TimeDatapoint(object):
    """MetricMeasure is a wrapper for a metric measurment."""

    def __init__(self, series, timestamp, labels, metric_name, metric):
        self._labels = labels
        self._series_name = series
        self._values = {
            metric_name: metric
        }
        self._timestamp = timestamp

class InfluxDBBackend(object):
    
    def __init__(self):
        INFLUX_DB_NAME = os.getenv('INFLUX_DB_NAME') or 'alakantti'
        INFLUX_URL = os.getenv('INFLUX_URL') or '127.0.0.1'
        INFLUX_PORT = os.getenv('INFLUX_PORT') or 8086
        INFLUX_USER = os.getenv('INFLUX_USER') or None
        INFLUX_PASSWORD = os.getenv('INFLUX_PASSWORD') or None
        
        self.client = InfluxDBClient(
            host=INFLUX_URL, port=INFLUX_PORT, 
            username=INFLUX_USER, password=INFLUX_PASSWORD, 
            database=INFLUX_DB_NAME
        )
        
        existing_databases = self.client.get_list_database()
        if INFLUX_DB_NAME not in list(db['name'] for db in existing_databases):
            print(INFLUX_DB_NAME)
            # Create if doesn't exist
            self.client.create_database(INFLUX_DB_NAME) 
        
    def metric_to_influx_datapoint(self, metric):
        influx_datapoint = {
            "measurement": metric._series_name,
            "tags": metric._labels,
            "time": metric._timestamp.isoformat(),
            "fields": metric._values,
        }
        return influx_datapoint

    def get_metric_for_date_range(self, grid_id, start, end):
        query_string = """
            SELECT "count" FROM 
                "telia-data"."autogen"."footfall" 
                WHERE grid_id='%s' AND time >= '%s' and time < '%s' FILL(null);
            """ % (grid_id, start.strftime("%Y-%m-%d 00:00:00"), end.strftime("%Y-%m-%d 00:00:00"))
        return self.client.query(query_string)

    def push_metric(self, metric):
        # Influx DB will overwrite two points which have the same 
        # timestamp and lables. No need for current pointer checks
        influx_datapoint = self.metric_to_influx_datapoint(metric)
        self.client.write_points([influx_datapoint])
        
    def push_metrics(self, metrics):
        # Influx DB will overwrite two points which have the same 
        # timestamp and lables. No need for current pointer checks
        influx_datapoints = [self.metric_to_influx_datapoint(metric) for metric in metrics]
        self.client.write_points(influx_datapoints)