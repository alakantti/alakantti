from uplink import Consumer, get, headers, Path, Query

class JCDecaux(Consumer):
    """A Python Client for the CodeClimate."""

    def __init__(self):
        Consumer.__init__(self, base_url="https://digi.jcdecaux.fi/")
        self.base_url = "https://digi.jcdecaux.fi/"

    @get("search/gethubs")
    def get_hubs(self): pass
    """Retrieves all."""
    
    @get("/search/gethubframeinfo")
    def get_frame_info(self, hub_id: Query("hubId")): pass
    """Retrieves repositories reports."""
    
