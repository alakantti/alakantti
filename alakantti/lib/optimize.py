#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 24 13:39:13 2018

Function optimize_ad(start_date,end_date,max_price)

Expects dates as a datetime.date object, max_price as an integer

Returns a list of dictionaries of the selected time slots with the following content
* name (string) the name of the billboard (from JCDecaux)
* date (datetime.datetime) the starting date and time of the time slot reserved
* time (string) either 'morning', 'day' or 'evening' describing verbally the time of the slot
* price (int) the price of the selected time slot
* footfall_count (int) total footfall count in the grid of the billboard
* object_id (int) is the object ID in the ArcGIS data

@author: tuomo
"""

#%%
import datetime
import json
import cvxpy
import numpy as np
from sklearn.externals import joblib


def optimize_ad(start_date,end_date,max_price):
    # Sanity check for inputs
    if not isinstance(start_date,datetime.date) or not isinstance(
            end_date,datetime.date):
        raise ValueError('use datetime.date inputs for start and end date')
    if not isinstance(max_price,int):
        raise ValueError('use int input for max_price')
    
    # This is the base for full dataset
    all_data = list()
    
    # This is a helper 'function' to get the variable name from the hour of the day
    day_variables = dict()
    day_variables[6] = 'morning'
    day_variables[12] = 'day'
    day_variables[18] = 'evening'
    
    # Get predictions in
    #trunk/optimization/
    with open('trunk/optimization/footfall_forecast_1week_5locs.pkl', 'rb') as file:
        predict_data = joblib.load(file)
    
    predict_data_start = datetime.date(2018,3,19)
    predict_data_end = datetime.date(2018,3,25)
    
    if start_date < predict_data_start:
        start_date = predict_data_start
    if end_date > predict_data_end:
        end_date = predict_data_end
    # list of billboards, which have a list of timeslots
    
    predict_data_start = datetime.datetime.combine(predict_data_start,
                                                   datetime.time(0, 0))
    predict_data_end = datetime.datetime.combine(predict_data_end+datetime.timedelta(days=1),
                                                   datetime.time(0, 0))
    # Get the json data in trunk/optimization/
    with open('trunk/optimization/footfall_data.json', 'r') as outfile:
        footfall_data = json.load(outfile)
    with open('trunk/optimization/jcdecaux.json', 'r') as outfile:
        billboard_data = json.load(outfile)
    
    # Create datetime.datetime variables for the timeslot
    timeslot_start = datetime.datetime.combine(start_date, 
                              datetime.time(6, 0))
    optimization_end = datetime.datetime.combine(end_date+datetime.timedelta(days=1), 
                              datetime.time(0, 0))
    
    #timeslot_idx = 0
    
    #%% Prepare the data
    while timeslot_start < optimization_end:
        # Calculate where the timeslot ends
        timeslot_end = timeslot_start + datetime.timedelta(hours=6)
        
        footfall_total = dict()
        for billboard_idx, billboard in enumerate(footfall_data):
            
            # Do some magic to get the data from the jsons
            billboard_name = billboard['billboard_name']
            
            # This is the data structure to fill
            footfall_total[billboard_name]=dict()
            
            # ****** This is the old calculation based on real data ****
            
            # This is not pretty... but it should be working
            # The timestamp data from json is milliseconds from epoch
            # we get seconds from epoch. Due to time zone differences we need 
            # to remove some seconds from the unix timesstam. And because GMT+2
            # was actually used to create the json data, we do it TWICE!
#            footfall_slots = list(filter(lambda x: 
#                (x['attributes']['time']/1000-2*7200) >= timeslot_start.timestamp() 
#                and (x['attributes']['time']/1000-2*7200) < timeslot_end.timestamp(),
#                    billboard['relatedRecordGroups'][0]['relatedRecords']))
#            
#            footfall_total[billboard_name]['count'] = sum(list(map(lambda x: 
#                x['attributes']['count_'],
#                footfall_slots)))
            
            # ****** This is the new calculation based on estimated data ****
            timeslot_idx = int(int((timeslot_start - predict_data_start).seconds/3600))
            timeslot_idx += int((timeslot_start - predict_data_start).days * 4 * 6)
            #print(str(timeslot_idx_new)+"="+str(timeslot_idx))
            
            timeslot_idxs = list(map(lambda x: x+timeslot_idx,range(6)))
            footfall_total[billboard_name]['count'] = 0
            for list_idx in timeslot_idxs:
                footfall_total[billboard_name]['count'] += predict_data[billboard_idx][list_idx]
            footfall_total[billboard_name]['count'] = int(footfall_total[billboard_name]['count'])
            
            # Add the grid object ID
            footfall_total[billboard_name]['object_id'] = billboard['relatedRecordGroups'][0]['objectId']
            
            # This is the JCDecaux data
            billboard_row = list(filter(lambda x: 
                x['name'] == billboard_name,billboard_data))[0]
            
            # Now we have the data, let's just make it nicer
            weekday = timeslot_start.weekday()
            day_variable = day_variables[timeslot_start.hour]
            allday_impressions = billboard_row['impressions']['days'][weekday]['allDay']
            
            #TODO how to select number of impressions?
            #JCDecaux default is the following, but it's unfair for the billboards
            #selected_number_of_impressions = int(1/6*billboard_row['impressions']['days'][weekday][day_variable])
            
            # Use 1000 for now.
            selected_number_of_impressions = 1000
            
            # Price for 1/6th of daily impressions
            std_price = billboard_row['price']
            
            # Calculate according to the JCDecaux 'formula'
            price = int((selected_number_of_impressions)/(allday_impressions)*std_price*6)
            
            footfall_total[billboard_name]['price'] = price
            footfall_total[billboard_name]['timeslot_start'] = timeslot_start
        all_data.append(footfall_total)
        
        
        #timeslot_idx += 6
        # Start next round from the end of this round
        if timeslot_end.hour == 0:
            timeslot_end += datetime.timedelta(hours=6)
            #timeslot_idx += 6
        timeslot_start = timeslot_end
        
    #%% Do the actual optimization functions
    
    # Create numpy arrays for cost and constraint
    footfall_count_list = np.array([])
    price_list = np.array([])
    
    # Gather the data to a 1D array from the 2D array we just created
    for timeslot in all_data:
        for billboard in timeslot:
            footfall_count_list = np.append(footfall_count_list,timeslot[billboard]['count'])
            price_list = np.append(price_list, timeslot[billboard]['price'])
    
    # Create variables for cvxpy
    selection = cvxpy.Variable(len(price_list), boolean=True)

    price_constraint = price_list * selection <= max_price
    total_footfall = footfall_count_list * selection
    
    knapsack_problem = cvxpy.Problem(cvxpy.Maximize(total_footfall), [price_constraint])
    
    # Solve the problem
    knapsack_problem.solve(solver=cvxpy.GLPK_MI)
    
    # Transform the results to a nicer array
    return_array = list([])
    i = 0
    for timeslot in all_data:
        for billboard in timeslot:
            if selection.value[i] == 1:
                day_variable = day_variables[timeslot[billboard]['timeslot_start'].hour]
                return_array.append({'name': billboard, 
                                     'date': timeslot[billboard]['timeslot_start'], 
                                     'time': day_variable, 
                                     'price': timeslot[billboard]['price'],
                                     'footfall_count': timeslot[billboard]['count'],
                                     'object_id':timeslot[billboard]['object_id']})
            i += 1
    return return_array

# Sample usage and debugging
if __name__ == "__main__":
    start_date = datetime.date(2018,3,19)
    end_date = datetime.date(2018,3,25)
    max_price = 3500
    
    my_array = optimize_ad(start_date,end_date,max_price)
    
    total_price = 0
    for ad in my_array:
        print("Choose "+ad['name']+" at "+ad['time']+" on "+ad['date'].strftime('%Y-%m-%d'))
        total_price += ad['price']
    print("Total price: "+str(total_price)+ " EUR")
    #print(my_array)