# -*- coding: utf-8 -*-
"""GeoGrid models."""
import datetime as dt

# from sqlalchemy import func
# 
# from alakantti.database import Column, Model, SurrogatePK, db, reference_col, relationship
# from geoalchemy2 import Geography

# class GeoGrid(Model, SurrogatePK):
#     """A Grid."""
# 
#     __tablename__ = 'geographic_grid'
#     grid_name = Column(db.String(124), nullable=False)
#     grid_id = Column(db.Integer)
#     polygons = Column(Geography('MULTIPOLYGON'))
# 
#     def __repr__(self):
#         """Represent instance as a unique string."""
#         return '<Grid({id!r})>'.format(id=self.id)
# 
#     @classmethod
#     def get_geo_grid(kls, latitude, longitude):
#         return db.session.query(GeoGrid).filter(
#             GeoGrid.polygons.ST_Contains('POINT(4 1)')
#             )