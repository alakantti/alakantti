# -*- coding: utf-8 -*-
"""User forms."""
from flask_wtf import FlaskForm

from wtforms import IntegerField, StringField, DateField, SelectMultipleField
from wtforms.validators import DataRequired, Email, EqualTo, Length, Optional
from wtforms.widgets import ListWidget, CheckboxInput

from datetime import date

from .models import Advertisemnt

class MultiCheckboxField(SelectMultipleField):
	widget = ListWidget(prefix_label=False)
	option_widget = CheckboxInput()


class AdvertisementForm(FlaskForm):
    """Register form."""

    name = StringField('Name', validators=[DataRequired(), Length(min=3, max=25)])
    budget = IntegerField('Budget', validators=[DataRequired()])
    
    earliest_start = DateField('Earliest Start', format='%d/%m/%Y', default=date(2018, 3, 19), validators=[DataRequired()])
    latest_end = DateField('Latest End', format='%d/%m/%Y', default=date(2018, 3, 25), validators=[DataRequired()])

    age_range_up = IntegerField('Age range Up', validators=[Optional()])
    age_range_low = IntegerField('Age range low', validators=[Optional()])
    
    gender = MultiCheckboxField('Gender', choices=[('M', 'Male'), ('F', 'Female')])
    
    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(AdvertisementForm, self).__init__(*args, **kwargs)
        self.advertisemnt = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(AdvertisementForm, self).validate()
        if not initial_validation:
            return False
        return True
		
class AdvertisementOptimizationForm(FlaskForm):
	pass
