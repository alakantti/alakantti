# -*- coding: utf-8 -*-
"""Ad models."""
import datetime as dt

from alakantti.database import Column, Model, db, relationship, SurrogatePK

class Advertisemnt(Model, SurrogatePK):
    """An Advertisemtn."""

    __tablename__ = 'advertisement'
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    owner_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    active = Column(db.Boolean(), default=False)
    
    earliest_start = Column(db.Date, nullable=False)
    latest_end = Column(db.Date, nullable=False)
    
    owner = db.relationship('User', backref=db.backref('advertisemnts', lazy=True))
        
    name = Column(db.String(124), nullable=False)
    budget = Column(db.Integer, nullable=False)
    
    def __repr__(self):
        """Represent instance as a unique string."""
        return '<Advertisemnt({id!r})>'.format(id=self.id)
        
