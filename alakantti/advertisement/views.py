# -*- coding: utf-8 -*-
"""User views."""
import datetime
import io
import csv
from datetime import timedelta
from itertools import zip_longest

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, current_user
from alakantti.advertisement.models import Advertisemnt
from alakantti.advertisement.forms import AdvertisementForm, AdvertisementOptimizationForm
from alakantti.utils import flash_errors, daterange

from alakantti.lib.optimize import optimize_ad
from alakantti.providers.models import JCDEcauxOutdoor

from alakantti.lib.influx_client import *

blueprint = Blueprint('advertisement', __name__, url_prefix='/ads', static_folder='../static')


GRID_DATABASE = {
    'Columbus Helsinki': 6305076,
    'Linnanmäki Helsinki': 6094125,
    'Töölöntori Helsinki': 6075376,
    'Aleksanterinkatu Helsinki': 6098821,
    'Metroasema Aalto-yliopisto': 5976925,
}

INV_GRID_DATABASE = {v: k for k, v in GRID_DATABASE.items()}

@blueprint.route('/')
@login_required
def list_ads():
    """List ads."""
    return render_template('advertisement/list.html')


@blueprint.route('/create/', methods=['GET', 'POST'])
def create():
    """Register new ad."""
    form = AdvertisementForm(request.form)
    if form.validate_on_submit():
        ad = Advertisemnt.create(
            owner=current_user,
            name=form.name.data, 
            budget=form.budget.data,
            earliest_start=form.earliest_start.data,
            latest_end=form.latest_end.data,
            active=True)
            
        flash('Awesome, now lets optimise it!', 'success')
        return redirect(url_for('advertisement.optimize', ad_id=ad.id ))
    else:
        flash_errors(form)
    return render_template('advertisement/create.html', form=form)
    

@blueprint.route('/<ad_id>', methods=['GET', 'POST'])
def view(ad_id):
    """Register new user."""
    advertisemnt = Advertisemnt.get_by_id(ad_id)
    return render_template('advertisement/view.html', advertisemnt=advertisemnt)
    
    
@blueprint.route('/<ad_id>/performance', methods=['GET', 'POST'])
def performance(ad_id):
    """Register new user."""
    advertisemnt = Advertisemnt.get_by_id(ad_id)
    return render_template('advertisement/performance.html', advertisemnt=advertisemnt)


@blueprint.route('/<ad_id>/optimize', methods=['GET', 'POST'])
def optimize(ad_id):
    """Register new user."""
    advertisemnt = Advertisemnt.get_by_id(ad_id)
    
    start_date = advertisemnt.earliest_start
    end_date = advertisemnt.latest_end
    max_price = advertisemnt.budget
    
    optimization_results = optimize_ad(
        start_date,
        end_date,
        max_price)
        
    date_results_dict = {}
    
    for single_date in daterange(start_date, end_date):
        date_string = single_date.strftime("%A %d/%m/%Y")
        date_results_dict[date_string] = []
        
        for result in optimization_results:
            if result['date'].date() == single_date:
                date_results_dict[date_string].append(result)
    
    selected_points = set(x['name'] for x in optimization_results)
    grid_ids = set(GRID_DATABASE[x] for x in selected_points)
    
    cli = InfluxDBBackend()
    data_series = []
    headers = ["Time"]
    for grid_id in grid_ids:
        query = cli.get_metric_for_date_range(grid_id ,start_date-timedelta(days=5), start_date)
        if not query.items():
            continue
        headers += [INV_GRID_DATABASE[grid_id]]
        results = query.items()[0][1]
        new_data = [str(x['count']) for x in results]
        data_series.append(new_data)
    
    # Populate with timestamps
    query = cli.get_metric_for_date_range(grid_id ,start_date-timedelta(days=7), start_date)
    results = query.items()[0][1]
    # 2018-03-18T07:00:00Z"
    # DD-MM-YYYY HH:mm:ss
    data_series = [
        [
            datetime.datetime.strptime(
                x['time'], 
                "%Y-%m-%dT%H:%M:%SZ").isoformat() for x in results]
        
    ] + data_series
    
    export_data = zip_longest(*data_series, fillvalue = '')
    
    # output = io.StringIO()
    # writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
    # writer.writerow(headers)
    # writer.writerows(export_data)
    # csv_string = output.getvalue()
    headers = [",".join(headers)]
    export_2 = [ ",".join(x) for x in export_data]
    
    datapoints = JCDEcauxOutdoor.get_all()
    
    # datapoints = []
    return render_template('advertisement/optimize.html', 
        export_data=list(headers + export_2),
        selected_points=selected_points,
        datapoints=datapoints, 
        date_results_dict=date_results_dict,
        advertisemnt=advertisemnt)