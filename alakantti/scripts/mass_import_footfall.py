import csv
import os
import progressbar
from datetime import datetime
from alakantti.lib.influx_client import TimeDatapoint, InfluxDBBackend

BATCH_SIZE = 50000

path = "trunk/footfall_data/"

filelist = os.listdir(path)

influx = InfluxDBBackend()

file_number = 0
batch_size = 0
batch = []

for x in filelist:
    if x.endswith(".txt"):
        print("FILE %s" % file_number)
        file_number += 1
        with open(path+x, newline='') as csvfile:
            datareader = csv.reader(csvfile, delimiter=',', quotechar='|')
            next(datareader, None)
            for row in progressbar.progressbar(datareader):
                grid_id = int(row[0])
                # 17.3.2018 8.00.00
                time = datetime.strptime(row[1], '%d.%m.%Y %H:%M:%S')
                count = int(row[2])
                #  series, timestamp, block_id, time, metric_name, metric
                data_point = TimeDatapoint('footfall', time, {'grid_id': grid_id}, 'count', count)
                batch.append(data_point)
                batch_size += 1
                if batch_size > BATCH_SIZE:
                    influx.push_metrics(batch)
                    batch_size = 0
                    batch = []