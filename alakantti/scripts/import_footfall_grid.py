import os
import geojson
import json
from alakantti.wisdom.models import GeoGrid
from shapely.geometry import shape

def import_footfall_grid():
    path = "trunk/footfall_data/grid/Uusimaa_footfall_grid.geojson"

    with open(path, 'r') as f:
        geo_data = geojson.loads(f.read())  
        
        for data in geo_data.features:
            s = json.dumps(data.geometry)
            g1 = geojson.loads(s)
            g2 = shape(g1)
            
            # MULTIPOLYGON(((0 0,4 0,4 4,0 4,0 0),(1 1,2 1,2 2,1 2,1 1)), ((-1 -1,-1 -2,-2 -2,-2 -1,-1 -1)))
            GeoGrid(
                grid_name='footfall', 
                grid_id=data.properties['id'],
                polygons=g2.wkt).save()
        


