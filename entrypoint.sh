#!/usr/bin/env bash

export FLASK_APP=alakantti.app
flask db migrate
flask db upgrade
gunicorn alakantti.app:create_app\(\) -b 0.0.0.0:5000 -w 3
