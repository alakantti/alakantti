#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 24 00:06:52 2018

This script fetches data from the five selected billboards to a CSV file.

@author: tuomo
"""
import requests
import ssl
import json
import urllib

# Fetch thse billboards
billboards_to_fetch=['Linnanmäki Helsinki',
                     'Töölöntori Helsinki',
                     'Aleksanterinkatu Helsinki',
                     'Metroasema Aalto-yliopisto',
                     'Columbus Helsinki']


ssl._create_default_https_context = ssl._create_unverified_context



# Get the general data (from all billboards)
url = 'https://digi.jcdecaux.fi/search/gethubs'
r = requests.get(url, verify=False)
general_data = json.loads(r.text)

#print(general_data)
# Get the specific data from selected billboards
url_base = 'https://digi.jcdecaux.fi/search/gethubframeinfo?&hubId='

full_data = []

for billboard in billboards_to_fetch:
    url = url_base + urllib.parse.quote(billboard)
    
    r = requests.get(url, verify=False)
    specific_data = json.loads(r.text)
    
    this_row = list(filter(lambda x: x['id']==billboard, general_data))[0]
    print(this_row)
    this_row['impressions'] = specific_data['impressions']
    full_data.append(this_row)

with open('jcdecaux.json', 'w') as outfile:
    json.dump(full_data, outfile)