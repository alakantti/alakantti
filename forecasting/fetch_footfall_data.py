#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 24 10:51:30 2018

Get data for interesting Object IDs from ArcGIS web API

@author: tuomo
"""
import requests
import ssl
import json

ssl._create_default_https_context = ssl._create_unverified_context

# The OBJECT_IDs of interesting places, looked up manually for now.
# Columbus Helsinki = 98446
# Linnanmäki Helsinki = 87739
# Töölöntori Helsinki = 86786
# Aleksanterinkatu Helsinki = 87987
# Metroasema Aalto-yliopisto = 81945


billboards = [{'name':'Columbus Helsinki','object_id':98446},
           {'name':'Linnanmäki Helsinki','object_id':87739},
           {'name':'Töölöntori Helsinki','object_id':86786},
           {'name':'Aleksanterinkatu Helsinki','object_id':87987},
           {'name':'Metroasema Aalto-yliopisto','object_id':81945}]

api_url_begin = 'https://services9.arcgis.com/Eg8N8SBgi1YS24tB/arcgis/rest/services/Uusimaa_Footfall_WFL1/FeatureServer/0/queryRelatedRecords?objectIds='
api_url_end = '&relationshipId=&outFields=count_%2C+time&definitionExpression=&orderByFields=&returnCountOnly=false&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&resultOffset=&resultRecordCount=&f=pjson&token=4FWsowjLqWHAp3docMQK577O-KBU5qLgC5yoJIXxqpyABfx_W9QcKEizHVaZJNZgU_pv18IakSuDRhgzNvUol718ImKKu_LKKoEM7Y6l0XwGHeK6D35xYN5kIjD7i7qmA5LWk2Z8rupiCQFYks9I8tsJ7htDcOkTNsYmsqjxyyaaT_JA_EeoFcsOIu4JrGMxosvn2YjA5vgP06VmDvN3Labx9SDNlnTdbMbjSshAbHk.'
full_data = []

for billboard in billboards:
    object_id = str(billboard['object_id'])
    
    # Start loop here
    url = api_url_begin + object_id + api_url_end
    r = requests.get(url, verify=False)
    parsed_json = json.loads(r.text)
    parsed_json['billboard_name'] = billboard['name']
    full_data.append(parsed_json)
    
with open('footfall_data.json', 'w') as outfile:
    json.dump(full_data, outfile)
