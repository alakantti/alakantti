import pandas as pd
import numpy as np
from statsmodels.tsa.arima_model import ARIMA
import matplotlib.pyplot as plt
import json
import statsmodels.graphics.tsaplots
from datetime import datetime

# load test data
with open("footfall_data.json", 'rb') as file:
    test_data = file.read()
data_all = json.loads(test_data)

# train models and make a week ahead out-of-sample forecast
pred_list = []
for j in range(0, len(data_all)):
    data = data_all[j]
    
    # find the relevant data from json
    data_json = data['relatedRecordGroups'][0]['relatedRecords']
    data_json[1]['attributes']['count_']
    data_json[1]['attributes']['time']
    
    # get data and time vectors
    count = []
    time = []
    for i in range(0, len(data_json)):
        count.append(data_json[i]['attributes']['count_'])
        time.append(data_json[i]['attributes']['time'])
    
    # sort lists in proper time order (initial order is wrong)
    time_sorted, count_sorted = zip(*sorted(zip(time, count)))
    
    # CREATE TRAINING AND TESTING DATASETS
    # skipping last week (6 days) of March due easter (no data to take into
    # account public holidays like that)
    # the second last week of March is left for model testing
    data_train = count_sorted[0:(len(count_sorted)-285)]
    data_test = count_sorted[(len(count_sorted)-285):len(count_sorted)]
    
    # get the months, days and hours for exogenous variables and fix everything
    # to UTC+2
    time_list = list(time_sorted)
    time_list[:] = [element - 7200000 for element in time_sorted]
    time_vec = []
    for i in range(0, len(time_sorted)):
        time_vec.append(datetime.utcfromtimestamp(time_list[i]/1000))
    
    time_train = time_vec[0:(len(time_vec)-285)]
    time_test = time_vec[(len(time_vec)-285):len(count_sorted)]    
    
    # get the exogenous variables
    exogenous_var_matrix = create_exogenous_variable_matrix(time_train)
    
    # seasonal difference
    hours_in_week = 168
    diff_data_train = create_diff_data(data_train, hours_in_week)
    
    plt.plot(data_train[0:168])
    plt.plot(diff_data_train[0:168])
    
    statsmodels.graphics.tsaplots.plot_acf(diff_data_train,lags=24)
    # this indicates AR is large...
    
    statsmodels.graphics.tsaplots.plot_pacf(diff_data_train,lags=24)
    # this indicates tha MA = 2

    # fit model (the parameters are defined in time_series_modelling.py)
    ma_params = [2,2,2,0,2]
    model = ARIMA(diff_data_train, order=(10,0,ma_params[j]),
                  exog=exogenous_var_matrix[168:len(exogenous_var_matrix)])
    model_fit = model.fit(disp=0)
    
    forecast_diff = model_fit.forecast(steps=168,
                                       exog=exogenous_var_matrix[0:168])[0]
    
    if len(data_train) != 1848:
        data_train = list(data_train)
        data_train.append(data_train[len(data_train)-1])
    B = np.reshape(data_train, (-1, 168))
    means_data = np.mean(B, axis=0)
    
    pred_i = []
    for i in range (0,len(forecast_diff)):
        pred_diff = forecast_diff[i]
        pred = pred_diff + means_data[i]
        pred_i.append(pred)    
    pred_list.append(pred_i)


##########################
##########################
# PLOTTING

# forecast vs last week vs test data
plt.figure(figsize=(16,6))
plt.plot(data_train[len(data_train)-168:len(data_train)], 'b--') # last week
plt.plot(data_test[0:168], 'g') # test data
plt.plot(pred_list[4], 'r.-') # prediction
plt.xlabel('date')
plt.ylabel('footfall')
plt.title(('1 week ahead out-of-sample forecast'))
plt.grid(True)
plt.savefig("predictions_vs_test_data_vs_last_week_.png")
plt.show()

# plot forecasted locations
plt.figure(figsize=(16,6))
pred1 = plt.plot(time_test[1:169], pred_list[0])
pred2 = plt.plot(time_test[1:169], pred_list[1])
pred3 = plt.plot(time_test[1:169], pred_list[2])
pred4 = plt.plot(time_test[1:169], pred_list[3])
pred5 = plt.plot(time_test[1:169], pred_list[4])
plt.xlabel('date')
plt.ylabel('footfall')
plt.title(('1 week ahead out-of-sample forecast'))
plt.grid(True)
plt.savefig("predictions_1week_5locs.png")
plt.show()

# save forecasting results
from sklearn.externals import joblib
joblib.dump(pred_list, 'footfall_forecast_1week_5locs.pkl')

from sklearn.externals import joblib
with open('footfall_forecast_1week_5locs.pkl', 'rb') as file:
    loaded_preds = joblib.load(file)

##########################
##########################


def create_exogenous_variable_matrix(time_train):
    # create dummy variables for weekdays
    month_vec = []
    day_vec = []
    hour_vec = []
    for i in range(0, len(time_train)):
        month_vec.append(time_train[i].month)
        day_vec.append(time_train[i].day)
        hour_vec.append(time_train[i].hour)
    day_type = []
    sat = []
    sun = []
    sat_jan = [6,13,20,27]
    sat_feb = [3,10,17,24]
    sat_mar = [3,10,17,24,31]
    sun_jan = [7,14,21,28]
    sun_feb = [4,11,18,25]
    sun_mar = [4,11,18,25]
    for i in range(0, len(time_train)):
        if (month_vec[i] == 1 and day_vec[i] in sat_jan) or (month_vec[i] == 2
            and day_vec[i] in sat_feb) or (month_vec[i] == 3 and day_vec[i] 
            in sat_mar):
                day_type.append(1)
                sat.append(1)
                sun.append(0)
        elif (month_vec[i] == 1 and day_vec[i] in sun_jan) or (month_vec[i] ==
             2 and day_vec[i] in sun_feb) or (month_vec[i] == 3 and day_vec[i]
             in sun_mar):
                day_type.append(2)
                sat.append(0)
                sun.append(1)
        else:
            day_type.append(0)
            sat.append(0)
            sun.append(0)
    # create dummy variables for parts of the day
    morning = []
    midday = []
    evening = []
    mor = [6, 7, 8, 9, 10, 11]
    mid = [12, 13, 14, 15, 16, 17]
    eve = [18, 19, 20, 21, 22, 23]
    for i in range(0, len(time_train)):
        if hour_vec[i] in mor:
                morning.append(1)
                midday.append(0)
                evening.append(0)
        elif hour_vec[i] in mid:
                morning.append(0)
                midday.append(1)
                evening.append(0)
        elif hour_vec[i] in eve:
                morning.append(0)
                midday.append(0)
                evening.append(1)
        else:
            morning.append(0)
            midday.append(0)
            evening.append(0)
    dum_dict = {'Sat': sat, 'Sun': sun, 'morning': morning, 'midday': midday, 'evening': evening}
    df = pd.DataFrame(dum_dict, columns=['Sat','Sun', 'morning', 'midday', 'evening'])
    return df


def create_diff_data(data_train, interval):
    # create diff data
	diff = list()
	for i in range(interval, len(data_train)):
		value = data_train[i] - data_train[i - interval]
		diff.append(value)
	return np.array(diff)
