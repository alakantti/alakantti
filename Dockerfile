FROM python:3.7

RUN apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash \
    && apt-get install nodejs -yq

COPY requirements requirements
RUN pip install numpy==1.15.4
RUN pip install -r requirements/prod.txt

COPY alakantti alakantti

COPY package.json package.json
RUN npm install
COPY webpack.config.js webpack.config.js
COPY assets assets
RUN npm run build

ENV FLASK_APP=autoapp.py
ENV FLASK_ENV=production
ENV FLASK_DEBUG=0

COPY ./trunk /trunk
COPY ./migrations /migrations
COPY ./entrypoint.sh /
EXPOSE 5000
ENTRYPOINT ["/entrypoint.sh"]
